import os
from time import sleep

from flask import Flask

app = Flask(__name__)

SECRET_NUMBER = os.getenv("SECRET_NUMBER", 42069)


@app.route("/return_secret_number", methods=["GET"])
def return_secret_number():
    sleep(0.99)
    return {"secret_number": SECRET_NUMBER}
